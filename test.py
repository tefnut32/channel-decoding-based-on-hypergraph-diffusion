# general imports
import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

# load required Sionna components
from sionna.fec.utils import load_parity_check_examples, LinearEncoder, gm2pcm
from sionna.utils.plotting import PlotBER
from sionna.fec.ldpc import LDPCBPDecoder

from gnn import * # load GNN functions
from wbp import * # load weighted BP functions

import tensorflow as tf
print(tf.config.list_physical_devices('GPU'))

gpus = tf.config.list_physical_devices('GPU')
print('Number of GPUs available :', len(gpus))
if gpus:
    gpu_num = 0 # Number of the GPU to be used
    try:
        #tf.config.set_visible_devices([], 'GPU')
        tf.config.set_visible_devices(gpus[gpu_num], 'GPU')
        print('Only GPU number', gpu_num, 'used.')
        tf.config.experimental.set_memory_growth(gpus[gpu_num], True)
    except RuntimeError as e:
        print(e)


#----- BCH -----
params={
    # --- Code Parameters ---
        "code": "BCH", # (63,45)
    # --- GNN Architecture ----
        "num_embed_dims": 20,
        "num_msg_dims": 20,
        "num_hidden_units": 40,
        "num_mlp_layers": 2,
        "num_iter": 8,
        "reduce_op": "mean",
        "activation": "tanh",
        "clip_llr_to": None,
        "use_attributes": False,
        "node_attribute_dims": 0,
        "msg_attribute_dims": 0,
        "use_bias": False,
    # --- Training ---- #
        "batch_size": [256, 256, 256], # bs, iter, lr must have same dim
        "train_iter": [35000, 300000, 300000],
        "learning_rate": [1e-3, 1e-4, 1e-5],
        "ebno_db_train": [3, 8.],
        "ebno_db_eval": 4.,
        "batch_size_eval": 10000, # batch size only used for evaluation during training
        "eval_train_steps": 1000, # evaluate model every N iters
    # --- Log ----
        "save_weights_iter": 10000, # save weights every X iters
        "run_name": "BCH_01", # name of the stored weights/logs
        "save_dir": "results/", # folder to store results
    # --- MC Simulation parameters ----
        "mc_iters": 1000,
        "mc_batch_size": 2000,
        "num_target_block_errors": 500,
        "ebno_db_min": 2.,
        "ebno_db_max": 9.,
        "ebno_db_stepsize": 1.,
        "eval_iters": [2, 3, 4, 6, 8, 10],
    # --- Weighted BP parameters ----
        "simulate_wbp": True, # simulate weighted BP as baseline
        "wbp_batch_size" : [2000, 2000, 2000],
        "wbp_train_iter" : [300, 10000, 2000],
        "wbp_learning_rate" : [1e-2, 1e-3, 1e-3],
        "wbp_ebno_train" : [5., 5., 6.],
        "wbp_ebno_val" : 7., # validation SNR during training
        "wbp_batch_size_val" : 2000,
        "wbp_clip_value_grad" : 10,
}


# all codes must provide an encoder-layer and a pcm
if params["code"]=="BCH":
    print("Loading BCH code")
    pcm, k, n, coderate = load_parity_check_examples(pcm_id=1, verbose=True)

    encoder = LinearEncoder(pcm, is_pcm=True)
    params["k"] = k
    params["n"] = n
else:
    raise ValueError("Unknown code type")


# ber_plot = PlotBER(f"GNN-based Decoding - {params['code']}, (k,n)=({k},{n})")
# ebno_dbs = np.arange(params["ebno_db_min"],
#                      params["ebno_db_max"]+1,
#                      params["ebno_db_stepsize"])



# uncoded QPSK
# e2e_uncoded = E2EModel(None, None, k=100, n=100) # k and n are not relevant here
# ber_plot.simulate(e2e_uncoded,
#                   ebno_dbs=ebno_dbs,
#                   batch_size=params["mc_batch_size"],
#                   num_target_block_errors=params["num_target_block_errors"],
#                   legend="Uncoded",
#                   soft_estimates=True,
#                   max_mc_iter=params["mc_iters"],
#                   forward_keyboard_interrupt=False,
#                   show_fig=False);


# simulate "conventional" BP performance for given pcm
# bp_decoder = LDPCBPDecoder(pcm, num_iter=20, hard_out=False)
# e2e_bp = E2EModel(encoder, bp_decoder, k, n)
# ber_plot.simulate(e2e_bp,
#                  ebno_dbs=ebno_dbs,
#                  batch_size=params["mc_batch_size"],
#                  num_target_block_errors=params["num_target_block_errors"],
#                  legend=f"BP {bp_decoder._num_iter.numpy()} iter.",
#                  soft_estimates=True,
#                  max_mc_iter=params["mc_iters"],
#                  forward_keyboard_interrupt=False,
#                  show_fig=False);

# train and simulate Weighted BP as additional baseline
# please note that the training parameters could be critical
# if params["simulate_wbp"]:
#     evaluate_wbp(params, pcm, encoder, ebno_dbs, ber_plot)


tf.random.set_seed(2) # we fix the seed to ensure stable convergence

# init the GNN decoder
gnn_decoder = GNN_BP(pcm=pcm,
                     num_embed_dims=params["num_embed_dims"],
                     num_msg_dims=params["num_msg_dims"],
                     num_hidden_units=params["num_hidden_units"],
                     num_mlp_layers=params["num_mlp_layers"],
                     num_iter=params["num_iter"],
                     reduce_op=params["reduce_op"],
                     activation=params["activation"],
                     output_all_iter=True,
                     clip_llr_to=params["clip_llr_to"],
                     use_attributes=params["use_attributes"],
                     node_attribute_dims=params["node_attribute_dims"],
                     msg_attribute_dims=params["msg_attribute_dims"],
                     use_bias=params["use_bias"])

e2e_gnn = E2EModel(encoder, gnn_decoder, k, n)






# init model and print summary
e2e_gnn(1, 1.)
e2e_gnn.summary()





train = True # remark: training takes several hours
if train:
    train_gnn(e2e_gnn, params)
else:
    # you can also load the precomputed weights
    load_weights(e2e_gnn, "weights/BCH_precomputed.npy")







for iters in params["eval_iters"]:
    # instantiate new decoder for each number of iter (otherwise no retracing)
    gnn_dec_temp = GNN_BP(pcm=pcm,
                          num_embed_dims=params["num_embed_dims"],
                          num_msg_dims=params["num_msg_dims"],
                          num_hidden_units=params["num_hidden_units"],
                          num_mlp_layers=params["num_mlp_layers"],
                          num_iter=iters,
                          reduce_op=params["reduce_op"],
                          activation=params["activation"],
                          output_all_iter=False,
                          clip_llr_to=params["clip_llr_to"],
                          use_attributes=params["use_attributes"],
                          node_attribute_dims=params["node_attribute_dims"],
                          msg_attribute_dims=params["msg_attribute_dims"],
                          use_bias=params["use_bias"])
    # generate new model
    model = E2EModel(encoder, gnn_dec_temp, k, n)
    model(1,1.) # init model
    # copy weights from trained decoder
    model._decoder.set_weights(gnn_decoder.get_weights())

    # and run the BER simulations
    ber_plot.simulate(model,
                     ebno_dbs=ebno_dbs,
                     batch_size=params["mc_batch_size"],
                     num_target_block_errors=500,
                     legend=f"GNN {iters} iter.",
                     soft_estimates=True,
                     max_mc_iter=params["mc_iters"],
                     forward_keyboard_interrupt=False,
                     show_fig=False);

ber_plot(xlim=[2,9],ylim=[1e-5,0.1]) # show final figure






ber_plot(xlim=[2,9],ylim=[1e-5,0.1]) # show final figure




# export results for pgf plots
col_names = ["uncoded",  "bp-20"]
if params["simulate_wbp"]:
    col_names.append("wbp-20")
for it in params["eval_iters"]:
    col_names.append("gnn-" + str(it))
export_pgf(ber_plot, col_names)