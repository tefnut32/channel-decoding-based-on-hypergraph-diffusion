![alt text](fig/purdue-cs-logo.jpg "Purdue-CS-Logo")

# Implicit Hypergraph Diffusion for Channel Decoding
**Member**: Shu Li, Siqi Miao, Xi Pang, Haoyu Wang, Mengbo Wang, Rongzhe Wei

> This is a course project of [CS 536 (Fall 2022)](https://gitlab.com/purdue-cs536/fall-2022/public).

## Abstract
Physical-layer network coding plays an essential role in wireless networks. E.g., Polar Codes is a critical technology in the fifth generation (5G). The recently proposed neural network-based decoder exhibits some advantages like low latency, high throughput, and one-shot. However, these methods either suffer from the curse of dimensionality or lacking expressiveness.<br> 
In this project, we first show that a recent proposed hypergraph diffusion neural operator, ED-HNN, is a special case of the GNN method, which reveals some theoretical guarantees for the GNN method. We also show that the GNN method suffers from the high memory-consumption due to the inner nature of unrolling network.<br>
 To tackle this problem, we introduce the Jacobian-Free Backpropagation (JFB) technique and propose the implicit Hypergraph Diffusion model with deeper network structure. In the experiment part, we train our model on BCH/Regular LDPC codes, comparing it with the classical BP, weighted BP, and the state-of-the-art neural network decoder.

**Keywords: channel decoding, graph neural networks, implicit models**

## Problem Statemnt
![alt text](fig/Picture1.png "background")

Physical-layer network coding plays an essential role in wireless networks. E.g., we know Polar Codes, which is a linear block error-correcting code, and it’s a critical technology in the 5G communication for signal transmission.
Channel decoding happens on the receiver side. It is used to recover channel coded packets,. Hence it is the basis of cellular systems.<br>
In a noisy channel, errors may occur during the transmission of data from information source to destination, so we need a method to detect these errors and then correct them.<br>
There exist many methods for channel decoding. For example, traditionally, there’s a weighted belief propagation method, and there’s a more recently proposed GNN-based method that triggers a lot of interest.

**Cons**<br>
Since GNN inherits the drawbacks of unrolling network, with more layers it will suffer from high-memory consumption. E.g. a k-layers graph neural network  will consume about k-times memory compared to a 1-layer graph neural network.<br>
In this case, if we want to train a graph neural network  with very high channel decoding accuracy, it may take up infinite memory resources, which is unacceptable.

## Implicit hypergraph-diffussion-based method
![alt text](fig/Picture5.png "theoratical gaurantee")
**Pros**<br>
1. better decoding accuracy
1. constant memory consumption

Implicit method makes it possible to train the graph neural network with infinite layers. It decouples the forward pass and backward pass of graph neural network. The upper section of the current figure shows a k-layer unrolling network. If we want to train a deeper network, we need to prepare much more memory resources.
The lower half of this graph shows an implicit network. Instead of adding more layers. We use the efficient Jacobian-Free Backpropagation (JFB) implementation, it indefinitely loops within the green dashed box until it converged. In this way, we can improve graph neural network’s accuracy with constant memory.<br>

## Results
The implementation is based on [Sionna](https://developer.nvidia.com/sionna), [Tensorflow](https://www.tensorflow.org). 
This repo is adapted from [gnn-decoder](https://github.com/NVlabs/gnn-decoder).
Since the limitation of our GPU resources, we only shows the results on BCH code.

![alt text](fig/Picture6.jpeg "Experiment")
|                  | #iterations | loss  | bit error rate | test duration |
|------------------|-------------|-------|----------------|---------------|
| GNN              | 635000      | 0.058 | 0.02182        | 47.09s        |
| Implicit GNN+JFB | 143000      | 0.026 | 0.01048        | 269.40s       |

Our implicit GNN-based method (cyan-blue line) has better performance than BP, weighted BP and previous GNNs (with less than 6 layers).<br> 
During the training process, our implicit GNN-based method also shows faster convergence rate. Last but not the least, our method has lower Bit Error Rate than naive GNN.

## Usage
Run the scripts in `GNN_decoder_BCH.ipynb`.

## Reference
1. Cammerer, S., Hoydis, J., Aoudia, F. A., & Keller, A. (2022). Graph Neural Networks for Channel Decoding. arXiv preprint arXiv:2207.14742.<br>
2. Wang, P., Yang, S., Liu, Y., Wang, Z., & Li, P. (2022). Equivariant Hypergraph Diffusion Neural Operators. arXiv preprint arXiv:2207.06680.<br>
3. Fung, S.W., Heaton, H., Li, Q., McKenzie, D., Osher, S. and Yin, W., 2022. JFB: Jacobian-free backpropagation for implicit networks. In Proceedings of the AAAI Conference on Artificial Intelligence.

## License
Copyright © 2022, Purdue University. All rights reserved.<br>
This work is made available under the MIT License.

Last update by Dec 09, 2022.